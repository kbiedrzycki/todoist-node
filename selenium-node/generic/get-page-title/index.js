module.exports = async function (browser, { inputParameters, secretParameters }) {
  console.log(inputParameters, secretParameters);

  if (!inputParameters.testTargetUrl) {
    throw 'Invalid event data!';
  }

  await browser.get(inputParameters.testTargetUrl);

  return { pageTitle: await browser.getTitle() };
};
