module.exports = async function (browser, { inputParameters, previousResult }) {
  if (!inputParameters.testTargetUrl || !previousResult.pageTitle) {
    throw 'Invalid event data!';
  }

  const page = await browser.newPage();

  await page.goto(inputParameters.testTargetUrl);

  await page.click('.add-task__text');
  await (await page.$('.add-task__content')).type(`Page to read: "${previousResult.pageTitle} - ${inputParameters.testTargetUrl}"`);
  await page.click('.add-task__submit');
  await page.waitFor(() => !document.querySelector('.add-task__content'));

  return { created: true, taskTitle: inputParameters.taskTitle };
};
